<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
</head>
<body>
	<h1>${message}</h1>

	<!-- 投稿登録 -->
	<form:form action="${pageContext.request.contextPath}/home/insert"
		modelAttribute="TodoList">
		<form:input path="todo" />
		<form:input path="dueDate" type="date" />
		<input type="submit" value="Create Todo">
	</form:form>

		<!-- 投稿登録 -->
	<form:form action="${pageContext.request.contextPath}/home/search"
		modelAttribute="TodoList">
		<input type="checkbox" name="is_completed" value="1">完了済  /
		<input type="checkbox" name="is_completed" value="0">未完了
		<form:input path="searchTodo" />
	<!--<form:input path="searchTodo" type="date"/>-->
		<input type="submit" value="SEARCH">
		<br />
	</form:form>

	<ul>
		<c:forEach items="${lists}" var="list">
			<br />
				<!-- タスク状態変更ボタン 完了ボタン-->
				<form:form
					action="${pageContext.request.contextPath}/home/completed"
					modelAttribute="TodoList">
					<form:hidden path="id" value="${list.id}" />
					<c:if test="${list.isCompleted != 0}">
						<input type="hidden" name="is_completed" value="1" />
						<input type="submit" value="Completed">
					</c:if>

					<c:if test="${list.isCompleted == 0}">
						<input type="hidden" name="is_completed" value="0" />
						<input type="submit" value="unfinished">
					</c:if>
				</form:form>
				<!--  表示 -->

				<li>
				<c:out value="${list.todo}"></c:out>:
				<c:out value="${list.dueDate}"></c:out>:
				<c:out value="${list.updatedDate}"></c:out>

				<!-- 編集ボタン -->
				<form:form
					action="${pageContext.request.contextPath}/home/update"
					modelAttribute="TodoList">
					<form:hidden path="id" value="${list.id}" />
					<form:input path="todo" />
					<form:input path="dueDate" type="date" />
					<input type="submit" value="Update">
				</form:form>

				<!-- 削除ボタン -->
				<form:form
					action="${pageContext.request.contextPath}/home/delete"
					modelAttribute="TodoList">
					<form:hidden path="id" value="${list.id}" />
					<input type="submit" value="Delete">
				</form:form><br />
		</c:forEach>
	</ul>





</body>
</html>