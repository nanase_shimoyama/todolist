package jp.co.todolist.dto.test;

import java.util.Date;

public class TodoListDto {

    private Integer id;
    private String todo;
    private int isCompleted;
    private Date updatedDate;
    private Date createdDate;
    private Date dueDate;
    private String searchTodo;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTodo() {
		return todo;
	}
	public void setTodo(String todo) {
		this.todo = todo;
	}
	public int getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(int isCompleted) {
		this.isCompleted = isCompleted;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getSearchTodo() {
		return searchTodo;
	}
	public void setSearchTodo(String searchTodo) {
		this.searchTodo = searchTodo;
	}

}
