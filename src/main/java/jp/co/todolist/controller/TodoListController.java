package jp.co.todolist.controller;

import java.util.List;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.todolist.dto.test.TodoListDto;
import jp.co.todolist.entity.TodoList;
import jp.co.todolist.service.TodoListService;
@Controller
public class TodoListController {

    @Autowired
    private TodoListService todolistService;

    @RequestMapping(value = "/home/{id}", method = RequestMethod.GET)
    public String home(Model model, @PathVariable int id) {
        TodoListDto idList = todolistService.getId(id);
        model.addAttribute("title", "MyBatisのサンプルです");
        model.addAttribute("lists", idList);
        return "home";
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET, produces="text/plain;charset=utf-8")
    public String homeAll(Model model) {

    	TodoList todoList = new TodoList();
        List<TodoListDto> allList = todolistService.getTodoListAll();
        model.addAttribute("title", "ToDo List");
        model.addAttribute("lists", allList);
        model.addAttribute("TodoList",todoList);
        return "home";
    }

    // insertList(TodoList todo)

    @RequestMapping(value = "/home/insert", method = RequestMethod.POST, produces="text/plain;charset=utf-8")
    public String insertList(@Valid @ModelAttribute("TodoList") TodoList form ,BindingResult result,Model model) {

    	if (result.hasErrors()) {
            model.addAttribute("message", "エラー");

        	//TodoList todoList = new TodoList();
            List<TodoListDto> allList = todolistService.getTodoListAll();
            model.addAttribute("title", "ToDo List");
            model.addAttribute("lists", allList);
            model.addAttribute("TodoList",form);

            return "./home";
        }

    	todolistService.insertList(form);
        return "redirect:./";
    }

	//DELETE
	@RequestMapping(value = "/home/delete", method = RequestMethod.POST, produces="text/plain;charset=utf-8")
	public String deleteList(@ModelAttribute TodoList form, Model model) {
		int count = todolistService.deleteList(form.getId());
		Logger.getLogger(TodoListController.class).log(Level.INFO, "削除件数は" + count + "件です。");
		return "redirect:./";
	}

	//EDIT
	@RequestMapping(value = "/home/update", method = RequestMethod.POST, produces="text/plain;charset=utf-8")
	public String updateList(@ModelAttribute("TodoList") TodoList form ,BindingResult result,Model model) {
	    TodoListDto dto = new TodoListDto();
	    BeanUtils.copyProperties(form, dto);
		todolistService.updateList(dto);
//		Logger.getLogger(TodoListController.class).log(Level.INFO, "編集件数は" + count + "件です。");
		return "redirect:./";
	}

	//TASK completed or Not
	@RequestMapping(value = "/home/completed", method = RequestMethod.POST, produces="text/plain;charset=utf-8")
	public String completedList(@ModelAttribute TodoList form, Model model) {
		int count = todolistService.completedList(form.getId());
		Logger.getLogger(TodoListController.class).log(Level.INFO, count + "の状態です");
		return "redirect:./";
	}

    @RequestMapping(value = "/home/search", method = RequestMethod.POST)
    public String searchList(@ModelAttribute("TodoList") TodoList form ,BindingResult result,Model model) {
       // TodoList searchList = todolistService.getSearchList(form);
        List<TodoListDto> allSearchList = todolistService.getSearchList(form);
        model.addAttribute("lists", allSearchList);
        model.addAttribute("title", "Searched Todo List");

        TodoList todoList = new TodoList();
        model.addAttribute("TodoList",todoList);

        return "./home";
    }
}

