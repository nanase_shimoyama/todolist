package jp.co.todolist.mapper;

import java.util.List;

import jp.co.todolist.dto.test.TodoListDto;
import jp.co.todolist.entity.TodoList;

public interface TodoListMapper {
	TodoList getId(Integer id);
	List<TodoList> getTodoListAll();
	int insertList(TodoList todo);
	int deleteList(int id);
	void updateList(TodoListDto dto);
	int completedList(int id);
	List<TodoList> getSearchList(TodoList form);
	}
