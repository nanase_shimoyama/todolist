package jp.co.todolist.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class TodoList implements Serializable{

	private static final long serialVersionUID = 1L;

    private Integer id;

    @NotEmpty(message = "入力してください")
	@Size(min = 1, max = 50, message = "50文字以内で入力してください")
    private String todo;

    private int isCompleted;
    private Date updatedDate;
    private Date createdDate;

    @NotNull(message = "〆切を入力してください")
    private Date dueDate;
    private String searchTodo;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTodo() {
		return todo;
	}
	public void setTodo(String todo) {
		this.todo = todo;
	}
	public int getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(int isCompleted) {
		this.isCompleted = isCompleted;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getSearchTodo() {
		return searchTodo;
	}
	public void setSearchTodo(String searchTodo) {
		this.searchTodo = searchTodo;
	}

}
