package jp.co.todolist.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.todolist.dto.test.TodoListDto;
import jp.co.todolist.entity.TodoList;
import jp.co.todolist.mapper.TodoListMapper;

@Service
public class TodoListService {

    @Autowired
    private TodoListMapper todolistMapper;

    public TodoListDto getId(Integer id) {
        TodoListDto dto = new TodoListDto();
        TodoList entity = todolistMapper.getId(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public List<TodoListDto> getTodoListAll() {
        List<TodoList> allList = todolistMapper.getTodoListAll();
        List<TodoListDto> resultList = convertToDto(allList);
        return resultList;
    }

    private List<TodoListDto> convertToDto(List<TodoList> allList) {
        List<TodoListDto> resultList = new LinkedList<>();
        for (TodoList entity : allList) {
        	TodoListDto dto = new TodoListDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    public void insertList(TodoList todo) {
         todolistMapper.insertList(todo);
         return;
    }

    public int deleteList(int id) {
        int count = todolistMapper.deleteList(id);
        return count;
    }

    public void updateList(TodoListDto dto) {
        todolistMapper.updateList(dto);
        return;
    }

    public int completedList(int id) {
        int count = todolistMapper.completedList(id);
        return count;
    }

    public List<TodoListDto> getSearchList(TodoList form) {
    	  List<TodoList> searchList = todolistMapper.getSearchList(form);
          List<TodoListDto> resultList = convertToDto(searchList);
          return resultList;
    }
}